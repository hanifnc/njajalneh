<?php

namespace App;

use Jenssegers\MongoDB\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class Berita extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'berita';
    protected $fillable = [
        'judul', 'media', 'nama_media', 'kategori', 
        'kata_kunci', 'tanggal_terbit', 'bahasa', 
        'file_url', 'deskripsi', 'konten', 
        'provinsi', 'kabupaten'
    ];
    protected $guarded = [];
    protected $casts = [
        'kategori' => 'array'
    ];
}
