<?php

namespace App;

use Jenssegers\MongoDB\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'provinces';
    protected $fillable = [
        'kode_provinsi', 'provinsi'
    ];
    protected $guarded = [];

}
