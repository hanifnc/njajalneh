
//Flot Pie Chart
$(function() {
    
    var data = [{
        label: "Series 0",
        data: 10,
        color: "#4f5467",
        
    }, {
        label: "Series 1",
        data: 1,
        color: "#00c292",
    }, {
        label: "Series 2",
        data: 3,
        color:"#01c0c8",
    }, {
        label: "Series 3",
        data: 30,
        color:"#fb9678",
    }];

    var plotObj = $.plot($("#flot-pie-chart"), data, {
        series: {
            pie: {
                innerRadius: 0.5,
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        color: null,
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });

});

