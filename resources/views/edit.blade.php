@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <div class="col-md-12 white-box">
        <div class="row" style="margin-bottom:10">
            <div class="col-md-4" style="border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:grey; padding-left:0; margin-left:30;">
                <h1 style="font-weight:bold">Berita</h1>  
            </div>
        </div>
        <form method="post" action="{{action('BeritaController@update', $berita->_id)}}">
        @csrf

        <div class="row">
            <div class="col-md-5" style="padding-left:30">
                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold">Judul</h5>
                    </div>
                    <div class="col-md-8 form-group" style="padding-top:7;">
                    <input type="text" class="form-control form-control-line" style="text-transform:capitalize" name="judul" value="{{$berita ->judul}}"> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold">Media</h5>
                    </div>
                    <div class="col-md-8 form-group" style="padding-top:7;"> 
                    <input type="text" class="form-control form-control-line" style="text-transform:capitalize" name="nama_media" value="{{$berita -> nama_media}}"> 
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold">Tanggal Terbit</h5>
                    </div>
                    <div class="col-md-8 form-group" style="margin-bottom:5;">
                    <input type="date" class="form-control form-control-line" name="tanggal_terbit" value="{{$berita -> tanggal_terbit}}">
                    </div>
                </div>
                
                @php if($berita->provinsi){
                    @endphp
                    <div class="row" id="wilayah">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold">Wilayah</h5>
                    </div>
                    <div class="col-md-8 form-group" style="padding-top:7;">
                    <input type="text" class="form-control form-control-line"  name="kabupaten" value="{{$berita -> kabupaten}}">
                    <input type="text" class="form-control form-control-line"  name="provinsi" value="{{$berita -> provinsi}}">
                    </div>
                    </div>
                    @php

                }else{
                    @endphp
                    <div class="row" id="wilayah">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold">Media</h5>
                    </div>
                    <div class="col-md-8 form-group" style="padding-top:7;">
                    {{$berita->media}}
                    </div>
                    </div>
                    @php
                }
                @endphp
                
                

                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold">Kategori</h5>
                    </div>
                    <div class="col-md-8 form-group" style="padding-top:7;">
                        <?php 
                            //$value = json_decode($berita->kategori, true);
                            //dd($berita->kategori);
                            $kategori = array("Politik", "Sosial", "Budaya", "Ekonomi", "Pemerintahan", "Kesehatan", "Lainnya");
                            $i = 0;
                            foreach($kategori as $ktg){
                                echo'<input id="checkbox'.$i.'" type="checkbox" style="margin-right:5;" value="'. $ktg .'"'; 
                                
                                foreach($berita->kategori as $key){
                                     if($key == $ktg) echo 'checked="true"';
                                }
                                echo ' name="kategori[]">
                                <label for="checkbox'.$i.'" style="margin-right:5" > ' . $ktg . ' </label>';
                                $i++;
                            }
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold;">Kata Kunci</h5>
                    </div>
                    <div class="col-md-8 form-group" style="padding-top:7;">
                    <input type="text" class="form-control form-control-line" name="kata_kunci" value="{{$berita -> kata_kunci}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold">Deskripsi</h5>
                    </div>
                    <div class="col-md-8 form-group" style="padding-top:7;">
                    <input type="text" class="form-control form-control-line"  name="deskripsi" value="{{$berita -> deskripsi}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight:bold">Bahasa</h5>
                    </div>
                    <div class="col-md-8 form-group" style="padding-top:7;">
                        {{$berita -> bahasa}}
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 form-group">
                        {{$berita->file_url}}
                    </div>
                </div>
                <div class="row">
                    
                </div>

            </div>
            <div class="col-md-7">
                <textarea class="form-control form-control-line" name="konten" style="padding:0; background-color:#F8F8F8;" rows="25">{{ $berita->konten }}
                </textarea>
            </div>
            <div class="col-md-6" style="margin-top:30">
                        <a href="/kelola" style="color:#2cabe3;margin-left:2%;">
                            Kembali
                        </a>
                        <button type="submit" class="btn btn-default btn-rounded" style="background-color:#2cabe3;color:white;margin-left:1%;">
                            Perbarui
                        </button>
                        </form>
                    </div>
            <div class="col-md-2">
                <form method="post" action="{{action('BeritaController@upload', $berita->_id)}}">
                @csrf
                <button type="submit" class="btn btn-default btn-rounded" style="background-color:#2cabe3;color:white;margin-left:1%;">
                    Ajukan
                </button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

