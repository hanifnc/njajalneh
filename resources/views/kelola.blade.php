@extends('layouts.master')
@section('content')

    <div class="container-fluid">
        <div class="col-md-12 white-box">
        <div class="chartist-tooltip"></div>

                <div class="col-md-7" style="border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:grey; padding-left:0; margin-left:30;">
                    <h1 style="font-weight:bold">Kelola Berita</h1>
                </div>
                <div class="col-md-4 pull-right">
                    <form method="get" class="form-group" action="{{ action('BeritaController@search')}}" role="search" style="margin-top:20">
                        <div class="input-group col-md-10" >
                            <input class="form-control" type="search" id="search" name="search" style="text-transform:capitalize" placeholder="Search here" > 
                            <span class="input-group-prepend">
                            
                                <button type="submit" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button>
                            </span> 
                        </div>
                    </form>
                </div>
                <script>
                function myFunction() {
                    var x = document.getElementById("search").value;
                    document.getElementById("mbox").style.background-color = "#deecff";
                    document.getElementById("mbox").innerHTML = "Searched:" + x + '<a href="/kelola" style="display:">Tampilkan semua</a>';
                }
                </script>
            <div class="col-md-8">
                <div class="col-md-12" style="margin-left:2%">
                    <div class="row" id="mbox" style="padding: 10 10; width:95%;margin:15 0 15;">
                        
                    </div>
                    <div class="row">
                        <a href="{{action('BeritaController@create')}}">
                            <div class="col-md-2 white-box" style="border:1px solid grey; margin-right:1%; width: 250; height:250;">
                                <i class="fa fa-plus fa-lg" style="display: flex; justify-content:center;padding-top:45%;padding-bottom:50%"></i>
                            </div> 
                        </a>
                            @foreach($beritas as $berita)
                            <?php $state = array("draft" => "orange", "diajukan" => "blue", "diterima" => 'green', 'ditolak' => 'red'); 
                            
                            ?>
                            <a href="{{ action('BeritaController@edit', $berita->_id) }}">
                            <div class="col-md-2 white-box" style="display:table;border:1px solid grey; margin-right:1%; 
                                        height:250;width:250;border-bottom-width:thick;border-bottom-color:{{$state[$berita->status]}}">
                            <div style="display: table-cell;vertical-align: middle;text-align:center;">
                            <h5 style="font-weight:bold; text-transform:uppercase"> {{ $berita->judul }}</h5>
                            {{ $berita->tanggal_terbit }}<br>
                            {{ $berita->nama_media }}
                            <br>
                            <form action="{{action('BeritaController@destroy', $berita->_id)}}" method="post" onclick="return confirm('Are you sure?')">
                                @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-circle" type="submit" style="margin-top:10"><i class="fa fa-trash-o"></i></button>
                            </form>
                            </div></div></a>
                            @endforeach            
                        <!-- 
                            
                            $state = array("draft" => "orange", "diajukan" => "blue", "diterima" => 'green', 'ditolak' => 'red');
                            
                                echo '<a href="'. route('BeritaController@edit', $berita->_id) .'">';
                                echo '<div class="col-md-2 white-box" style="display:table;border:1px solid grey; margin-right:1%; 
                                        height:250;width:250;border-bottom-width:thick;border-bottom-color:'. 
                                        $state[$berita->status] . '">';
                                echo '<div style="display: table-cell;vertical-align: middle;text-align:center;">';
                                
                                
                                echo '<h5 style="font-weight:bold">' . $berita->judul . '</h2><br>';
                                echo $berita->nama_media;

                                //echo '<br><a href="' . action('BeritaController@', $berita->id) . '" class="btn btn-warning">Edit</a>';
                                
                                // if($i < count($berita)){
                                //     echo $berita[$i] . ' <br>halo<br> ' . $berita[$i];
                                // }
                                //echo $berita->judul . '<br>' . $berita->nama_media . 
                                echo ''; 
                            }
                                  
                            //}
                            
                        ?>  -->
                       
                    </div>
                </div>
            </div>
                <div class="col-md-4" style="padding-left:35">
                
                    <div class="row">
                        <h2 style="margin-top:-10;margin-bottom:0;color:#2cabe3;">Filter Berita</h2>
                    </div>
                    <form method="get" class="form-group" action="{{ action('BeritaController@filter') }}">
                    @csrf
                    <div class="row">
                        Periode Berita    <br>
                        From
                        <input type="date" name="from_date" value="<?php print(date("Y-m-d")); ?>">
                        To
                        <input type="date" name="to_date" value="<?php print(date("Y-m-d")); ?>">
                        <br>
                        <br>
                        Status 
                        <select class="form-control" name="status">
                            <option value="" disabled selected hidden>Pilih Status</option>
                            <option value="draft">Draft</option>
                            <option value="diajukan">Diajukan</option>
                            <option value="diterima">Diterima</option>
                            <option value="ditolak">Ditolak</option>
                        </select>
                        
                        <br>
                        <br>
                    </div>
                    <div class="row">
                        Status <br>
                        <i class="fa fa-circle" style="color:orange"></i> Draft 
                        <i class="fa fa-circle" style="color:blue"></i> Diajukan 
                        <i class="fa fa-circle" style="color:green"></i> Diterima 
                        <i class="fa fa-circle" style="color:red"></i> Ditolak 
                    </div>
                    <div class="row" style="margin-top:50">
                        <button class="btn btn-default btn-rounded" style="background-color:#2cabe3;color:white;margin-left:60%;">
                            Submit
                        </button>
                    </div>

                </form>
                </div>    
            </div>
        </div>
         
    </div>
@stop