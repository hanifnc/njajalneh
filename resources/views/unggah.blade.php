@extends('layouts.master')
@section('head')
<link rel="stylesheet" href="../plugins/bower_components/dropify/dist/css/dropify.min.css">
@stop
@section('content')

    <div class="container-fluid">
        <div class="col-md-12 white-box">
            <div class="row" style="margin-bottom:10">
                <div class="col-md-11" style="border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:grey; padding-left:0; margin-left:30;">
                    <h1 style="font-weight:bold;align-text:center">Unggah Berita</h1>
                </div>
            </div>
            <form action="{{ route('berita.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-3">
                            Media
                        </div>
                        <div class="col-md-4 form-group">
                            <input type="radio" name="media" value="nasional" checked>
                            <label for="radio"> Nasional </label>
                        </div>
                        <div class="col-md-4 form-group" style="padding-left:0">
                            <input onclick="document.getElementById('provinsi').disabled = true; document.getElementById('kabupaten').disabled = true;" type="radio" name="media" value="internasional">
                            <label for="radio"> Internasional </label>
                        </div>
                        <br> 
                        <div class="col-md-12 form-group" style="">
                            <input type="text" class="form-control form-control-line" style="text-transform:capitalize" placeholder="Nama Media" name="nama_media"> 
                        </div>       
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-3" style="align-content:center;padding-top:5">
                        Tanggal Terbit
                        </div>
                        <div class="col-md-9 form-group">
                            <input type="date" id="publishDay" name="tanggal_terbit" value="<?php print(date("Y-m-d")); ?>">
                        </div>
                    </div>
                </div>
                
                <div class="row" style="margin-top:10">
                    <div class="col-md-6">
                        <div class="col-md-3" style="align-content:center;padding-top:5">
                        Judul
                        </div>
                        <div class="col-md-9 form-group">
                            <input type="text" class="form-control form-control-line" style="text-transform:capitalize" placeholder="Judul Berita" name="judul"> 
                        </div> 
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-3" style="align-content:center;padding-top:5">
                        Bahasa
                        </div>
                        <div class="col-md-9 form-group">
                            <select class="form-control" name="bahasa">
                                <option value="Indonesian">Bahasa Indonesia</option>
                                <option value="English">English</option>
                            </select> 
                        </div>
                    </div>
                </div>
                
                <div class="row" style="margin-top:10">
                    <div class="col-md-6">
                        <div class="col-md-3" style="align-content:center;padding-top:5">
                        Provinsi
                        </div>
                        <div class="col-md-9 form-group">
                            <select class="form-control dynamic" name="provinsi" id="provinsi" data-dependent="kabupaten">
                                <option value="" disabled selected hidden>Pilih Provinsi</option>
                                @foreach($provinces as $prov)
                                <option value="{{ $prov['provinsi'] }}" > {{$prov['provinsi']}}</option>
                                @endforeach
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-3" style="align-content:center;padding-top:5">
                        Kabupaten
                        </div>
                        <div class="col-md-9 form-group">
                            <select class="form-control dynamic" id="kabupaten" name="kabupaten">
                                <option value="" disabled selected hidden selected="selected">Pilih Kabupaten</option>
                                @foreach($regencies as $reg)
                                <option value="{{ $reg['nama_kab'] }}" > {{$reg['nama_kab']}}</option>
                                @endforeach
                            </select> 
                        </div>
                    </div>
                </div>
                
                <div class="row" style="margin-top:10">
                    <div class="col-md-6">
                        
                        <div class="col-md-3" style="align-content:center;padding-top:5">
                            Kategori
                        </div>
                        <br>
                        <div class="col-md-12 form-group">
                            <?php 
                                $kategori = array("Politik", "Sosial", "Budaya", "Ekonomi", "Pemerintahan", "Kesehatan", "Lainnya");
                                $i = 0;
                                foreach($kategori as $ktg){
                                    echo'<input id="checkbox'.$i.'" type="checkbox" style="margin-right:5;" value="'. $ktg .'" name="kategori[]">
                                    <label for="checkbox'.$i.'" style="margin-right:5" > ' . $ktg . ' </label>';
                                    $i++;
                                    if($i%5 == 0){
                                        echo '<br>';
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        
                        <div class="col-md-3">
                        Unggah File Berita
                        </div>
<!-- 
                        <script>
                            $(document).ready(function(){
                                $('select[name="provinsi"]').on('change', function() {
                                    var provID = $(this).val();
                                    if(provID) {
                                        $.ajax({
                                            url: '/myform/ajax/'+provID,
                                            type: "GET",
                                            dataType: "json",
                                            success:function(data) {

                                                
                                                $('select[name="kabupaten"]').empty();
                                                $.each(data, function(key, value) {
                                                    $('select[name="kabupaten"]').append('<option value="'+ key +'">'+ value +'</option>');
                                                });


                                            }
                                        });
                                    }else{
                                        $('select[name="kabupaten"]').empty();
                                    }
                                });
                            });
                        </script> -->
                        <div class="white-box" style="padding:20 15 5 15">
                            <input type="file" id="input-file-now" name="fileurl" class="dropify"  /> 
                        </div>
                    </div>
                        
                </div>
                
                <div class="row" style="margin-top:10">
                    <div class="col-md-12">
                        <div class="col-md-12 form-group">
                            Kata Kunci
                            <br>
                            <input type="text" class="form-control form-control-line" placeholder="Kata Kunci" name="kata_kunci">
                        </div>
                    </div>
                </div>
                
                <div class="row" style="margin-top:10">
                    <div class="col-md-12">
                        <div class="col-md-12 form-group">
                        Deskripsi
                        <br>
                        <input type="text" class="form-control form-control-line" placeholder="Deskripsi" name="deskripsi">
                        </div>
                    </div>
                    
                </div>
                
                <div class="row" style="margin-top:10">
                    <div class="col-md-12">
                    <div class="col-md-12 form-group" >
                    
                    <textarea class="form-control form-control-line" name="konten" rows="5"></textarea>
                    </div>
                    </div>
                </div>

                <div class="row" style="margin-top:10">
                    <div class="form-group col-md-2" style="margin-left:80%;margin-top:10">
                        <a href="/kelola" >Batalkan</a>
                        <button type="submit" value="submit" class="btn btn-default btn-rounded" style="background-color:#2cabe3;color:white;margin-left:10;">
                                Submit
                        </button>
                    </div>
                </div>

            </form>
        </div>
       
        
    </div>

@stop
@section('plugins')
<script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
 <script>   
 $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
    @stop