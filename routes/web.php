<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('kelola', 'BeritaController@index')->name('berita.index');

Route::get('unggah', 'BeritaController@create')->name('berita.create');

Route::post('unggah', 'BeritaController@store')->name('berita.store');
Route::get('{_id}/edit', 'BeritaController@edit')->name('berita.edit');
Route::post('{_id}', 'BeritaController@update')->name('berita.update');
Route::post('{_id}/uploaded', 'BeritaController@upload')->name('berita.upload');

Route::delete('{_id}','BeritaController@destroy');

Route::get('/kelola/s','BeritaController@search');
Route::get('/kelola/f','BeritaController@filter');
Route::get('/dashboard','BeritaController@show');


